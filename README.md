# linux-autostart

Script that starts automatically on boot

### How to install

Just put the script in the **/etc/init.d/** folder and run the following commands:


`update-rc.d SCRIPTNAME defaults`

AND

`update-rc.d SCRIPTNAME enable`


Finished. It now should run on every reboot